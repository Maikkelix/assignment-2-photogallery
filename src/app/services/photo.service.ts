import { Injectable } from '@angular/core';
import {
  Plugins, CameraResultType, Capacitor, FilesystemDirectory,
  CameraPhoto, CameraSource
} from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { Aphoto } from '../aphoto';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
const { Camera, Filesystem, Storage } = Plugins;

const API_URL = environment.APIUrl;
const API_KEY = environment.APIKey;
const secretKey = environment.secretKey;

@Injectable({
  providedIn: 'root'
})

export class PhotoService {

  public photos: Photo[] = [];
  private PHOTO_STORAGE: string = "photos";
  private platform: Platform;

  constructor(platform: Platform,
    /*private http: HttpClient*/) {
    this.platform = platform;   
  }

  public async addNewToGallery() {
    // Take a photo
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });

    const savedImageFile = await this.savePicture(capturedPhoto);

    // Adding taken photo to Photos array
    this.photos.unshift(savedImageFile);

    // Cache photo data for later use
    Storage.set({
      key: this.PHOTO_STORAGE,
      value: this.platform.is("hybrid")
        ? JSON.stringify(this.photos)
        : JSON.stringify(this.photos.map(p => {
          // Base64 representation of the photo data
          // doesn´t need to be saved since it
          // is already saved on the Filesystem
          const photoCopy = { ...p };
          delete photoCopy.base64;

          return photoCopy;
        }))
    });
  }
/*
// Processing URL, APIKEY and additional secret key into address
// fetched from the https://unsplash.com to provide it
// to the Photogallery page (tab2)
// Not functioning correctly
  getData(url): Observable<any> {
    const address = `${API_URL}/${url}&apikey=${API_KEY}/${url}&secretKey=${secretKey}`;
    return this.http.get(address);
  }
  */

  public async loadSaved() {
    const photos = await Storage.get({ key: this.PHOTO_STORAGE });
    this.photos = JSON.parse(photos.value) || [];

    if (!this.platform.is('hybrid')) {
      for (let photo of this.photos) {
        // Read each saved photo's data from the Filesystem
        const readFile = await Filesystem.readFile({
          path: photo.filepath,
          directory: FilesystemDirectory.Data
        });

        // Web platform only: Save the photo into the base64 field
        photo.base64 = `data:image/jpeg;base64,${readFile.data}`;
      }
    }
  }

  /*
  // To be able to process photos´, one way is
  // to use Capasitor.

  // Use the device camera to take a photo:
  // https://capacitor.ionicframework.com/docs/apis/camera
  
  // Store the photo data into permanent file storage:
  // https://capacitor.ionicframework.com/docs/apis/filesystem
  
  // Store a reference to all photo filepaths using Storage API:
  // https://capacitor.ionicframework.com/docs/apis/storage
  */

  // Saving picture to the device´s memory
  private async savePicture(cameraPhoto: CameraPhoto) {
    // Convert photo´s format to base64, since the Filesystem 
    // API requires it during saving
    const base64Data = await this.readAsBase64(cameraPhoto);

    // Writing the file to the data directory
    const fileName = new Date().getTime() + '.jpeg';
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: FilesystemDirectory.Data
    });

    if (this.platform.is('hybrid')) {
      // Displaying the image by rewriting the
      // 'file://' path to the HTTP
      // Details: https://ionicframework.com/docs/building/webview#file-protocol
      return {
        filepath: savedFile.uri,
        webviewPath: Capacitor.convertFileSrc(savedFile.uri),
      };
    }
    else {
      // Usign webPath to display the image instead of base64
      // since it´s already loaded into device´s memory
      return {
        filepath: fileName,
        webviewPath: cameraPhoto.webPath
      };
    }
  }

  // Reading camera photo into base64 format based on the
  // platform the app is running on
  private async readAsBase64(cameraPhoto: CameraPhoto) {
      // "hybrid" task is to detect wheter system is using
      // Cordova or Capacitor
    if (this.platform.is('hybrid')) {
      // Reading the file base64 format
      const file = await Filesystem.readFile({
        path: cameraPhoto.path
      });

      return file.data;
    }
    else {
      // Fetching the photo, reading it as a blob and converting
      // it to base64 format
    const response = await fetch(cameraPhoto.webPath!);
    const blob = await response.blob();

    return await this.convertBlobToBase64(blob) as string;
    }
  }

  // Deleting picture by removing it from reference
  // data and the filesystem
  public async deletePicture(photo: Photo, position: number) {
    // Removing photo from the Photos refence data array
    this.photos.splice(position, 1);
  
    // Updating photos array cache by overwritng the existing photo array
    Storage.set({
      key: this.PHOTO_STORAGE,
      value: JSON.stringify(this.photos)
    });
      // Deleting photo file from the filesystem
      const filename = photo.filepath.substr(photo.filepath.lastIndexOf('/') + 1);
      await Filesystem.deleteFile({
        path: filename,
        directory: FilesystemDirectory.Data
      });
    }

  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });
}

interface Photo {
  filepath: string;
  webviewPath: string;
  base64?: string;
}
