import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', 
  loadChildren: './tabs/tabs.module#TabsPageModule' 
  //loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)},
  }];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
